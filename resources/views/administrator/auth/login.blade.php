@extends('administrator.layouts.guest')

@section('content')
    <div class="d-flex w-100 p-5">
        <div class="d-flex bg-white w-100" style="border-radius: 1.25rem; overflow-x: hidden; box-shadow: 0 1rem 2rem rgba(0,0,0,.05)">
            <div class="d-none d-md-block flex-fill">
                <div class="bg-light h-100"
                     style="background-image: url('https://picsum.photos/800/1000.webp?random={{ time() }}'); background-position: center; background-size: cover;">
                </div>
            </div>

            <div class="col px-5 d-flex align-items-center">
                <div>
                    <h1><strong>Авторизация</strong></h1>
                    <p class="text-secondary">Войдите используя логин и пароль от аккаунта</p>

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row mb-2">
                            <label for="email" class="col-md-5 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-2">
                            <label for="password" class="col-md-5 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-2">
                            <div class="col-md-6 offset-md-5">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex flex-column">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Login') }}
                            </button>

                            @if (\Illuminate\Support\Facades\Route::has('password.request'))
                                <a class="btn btn-link text-secondary" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
